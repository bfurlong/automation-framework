﻿using System.Collections.Generic;

namespace Understood.Selenium.Model
{
    public enum Gender
    {
        Girl,
        Boy
    }
    public class Child
    {

        public int Grade { get; set; }

        public string Name { get; set; }

        public Gender Gender { get; set; }

        public IEnumerable<ChildIssues> ChildIssues { get; set; } 
    }
}
