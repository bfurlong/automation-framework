﻿namespace Understood.Selenium.Model
{
    using SeleniumFramework.Model.UserModel;
    public class PosesUser : User
    {
        public string ZipCode { get; set; }
        public string FirstName { get; set; }
        public NotificationSettings NotificationSettings { get; set; }
    }
}
