﻿namespace Understood.Selenium.Model
{
    public enum ChildIssues
    {
        Reading,
        Math,
        Writing,
        Attention,
        Hyperactivity,
        Organization,
        Language,
        Listening,
        Social,
        Motor
    }
}
