﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Understood.Selenium.Model
{
    public class NotificationSettings
    {
        public Boolean NewsLetterSubscription { get; set; }
    }
}
