﻿using System;

namespace Understood.Selenium.Layouts
{
    using SeleniumFramework.Abstract;

    using Understood.Selenium.Pages.Core;

    using OpenQA.Selenium;

    public abstract class SingleColumnLayout : BasePage
    {
        public GlobalHeader GlobalHeader()
        {
            return new GlobalHeader(this);
        }

        public GlobalFooter GlobalFooter()
        {
            return new GlobalFooter(this);
        }

    }
}
