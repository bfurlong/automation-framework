﻿using Understood.Selenium.Layouts;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Model;
using Understood.Selenium.Pages.Registration;

namespace Understood.Selenium.Pages.Core
{
    using Understood.Selenium.Pages.Core.Partials;

    public class HomePage : SingleColumnLayout
    {
        [FindsBy(How = How.Id, Using = "hero_carousel_wrapper")]
        [CacheLookup]
        public IWebElement HeroRotater;

        [FindsBy(How = How.CssSelector, Using = ".button-guide-me")]
        [CacheLookup]
        public IWebElement HowCanWeHelpButton;

        [FindsBy(How = How.Id, Using = "main_0_lbCompleteMyProfile")]
        [CacheLookup]
        public IWebElement CompleteYourProfileLink;


        protected override By GetValidationErrorBy()
        {
            throw new NotImplementedException();
        }   

        public override void CreateNavigationInfo()
        {
            this.NavInfo = new NavigationInfo();
            this.NavInfo.ElementsRequiredToLoad.Add(this.HeroRotater);
        }

        public SignUp CompleteYourProfile()
        {
            HowCanWeHelpButton.Click();
            CompleteYourProfileLink.Click();
            return new SignUp();
        }

        public HowWeCanHelpOverlay HowCanWeHelp()
        {
            HowCanWeHelpButton.Click();
            return new HowWeCanHelpOverlay();
        }
    }
}
