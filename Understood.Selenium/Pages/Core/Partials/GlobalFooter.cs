﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Understood.Selenium.Pages.Core
{
    using SeleniumFramework.Abstract;

    public class GlobalFooter
    {
        private INavigatable currentPage;

        public GlobalFooter(INavigatable currentPage)
        {
            this.currentPage = currentPage;
        }
    }
}
