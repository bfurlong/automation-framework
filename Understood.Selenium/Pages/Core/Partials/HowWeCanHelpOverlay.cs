﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;
using SeleniumFramework.Utilities;

namespace Understood.Selenium.Pages.Core.Partials
{
    public class HowWeCanHelpOverlay
    {
        [FindsBy(How = How.Id, Using = "guideme-childissues-checkboxes")]
        [CacheLookup]
        public IWebElement IssueOptions;

        [FindsBy(How = How.Id, Using = "guideme-gradelevel-select")]
        [CacheLookup]
        public IWebElement GradeOptions;

        //TODO: Implement either abstract class or interface for partials

        public HowWeCanHelpOverlay()
        {
            PageFactory.InitElements(DriverManager.Instance,this);
        }

        public IEnumerable<IWebElement> IssueList()
        {
            return IssueOptions.FindElements(By.TagName("label"));
        }

        public IEnumerable<IWebElement> GradeList()
        {
            return GradeOptions.FindElements(By.TagName("button"));
        }

        public void SelectGrade(string grade)
        {
            foreach (var elem in this.GradeList().Where(elem => elem.Text.Contains(grade)))
            {
                elem.Click();
            }
        }
    }
}
