﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Model;
using System;
using Understood.Selenium.Layouts;
using Understood.Selenium.Model;

namespace Understood.Selenium.Pages.Registration
{
    
    public class SignUp : SingleColumnLayout
    {
        [FindsBy(How = How.Id, Using = "main_0_uxFirstName")]
        public IWebElement FirstNameInput;

        [FindsBy(How = How.Id, Using = "main_0_uxEmailAddress")]
        public IWebElement EmailInput;

        [FindsBy(How = How.Id, Using = "main_0_uxPassword")]
        public IWebElement PasswordInput;

        [FindsBy(How = How.Id, Using = "main_0_uxPasswordConfirm")]
        public IWebElement PasswordConfirmationInput;

        [FindsBy(How = How.Id, Using = "main_0_uxZipCode")]
        public IWebElement ZipCodeInput;

        [FindsBy(How = How.Id, Using = "main_0_uxNewsletterSignup")]
        public IWebElement NewsLetterCheckBox;

        [FindsBy(How = How.Id, Using = "main_0_uxSubmit")]
        public IWebElement FormSubmitButton;

        public TermsAndConditions RegisterUser(PosesUser user)
        {
            FirstNameInput.SendKeys(user.FirstName);
            EmailInput.SendKeys(user.Email);
            PasswordInput.SendKeys(user.Password);
            PasswordConfirmationInput.SendKeys(user.PasswordConfirm);
            //ZipCodeInput.SendKeys(user.ZipCode);
            FormSubmitButton.Click();

            return new TermsAndConditions();
        }

        protected override By GetValidationErrorBy()
        {
            throw new NotImplementedException();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = new NavigationInfo();
            this.NavInfo.Path = "/my-account/sign-up";
            this.NavInfo.ElementsRequiredToLoad.Add(FirstNameInput);
        }
        
        public SignIn SignIn()
        {
            return new SignIn();
        }
    }
}
