﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Abstract;
using SeleniumFramework.Model;
using SeleniumFramework.Model.UserModel;

namespace Understood.Selenium.Pages.Registration
{
  
    public class ForgotPassword : BasePage
    {
        [FindsBy(How = How.Id, Using = "main_0_txtEmailAddress")]
        public IWebElement EmailInput;

        [FindsBy(How = How.Id, Using = "main_0_btnSubmit")]
        public IWebElement SubmitButton;

        public ForgotPasswordConfirmation PasswordRequest(User user)
        {
            EmailInput.SendKeys(user.Email.ToString());
            SubmitButton.Click();
            return new ForgotPasswordConfirmation();
        }

        public void CancelPasswordRequest()
        {
            //TODO: Implement CancelPasswordRequest()
        }

        protected override By GetValidationErrorBy()
        {
            //TODO: Implement Get Validation Errors
            throw new NotImplementedException();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = new NavigationInfo();
            this.NavInfo.ElementsRequiredToLoad.Add(EmailInput);
            this.NavInfo.AddFindByCssClass(".sign-in-forgot-password");
            //this.NavInfo.AddFindByTextContains("Forgot");
        }
    }
}
