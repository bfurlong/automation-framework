﻿namespace Understood.Selenium.Pages.Registration
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using SeleniumFramework.Abstract;
    using SeleniumFramework.Model;

    public class TermsAndConditions : BasePage
    {
        [FindsBy(How = How.Id, Using = "main_0_btnAgree")]
        [CacheLookup]
        public IWebElement AcceptButton;

        [FindsBy(How = How.Id, Using = "main_0_btnNotAgree")]
        public IWebElement DisagreeButton;

        public void AcceptTerms()
        {
            AcceptButton.Click();
        }

        public void RejectTerms()
        {
            DisagreeButton.Click();
        }

        protected override By GetValidationErrorBy()
        {
            throw new System.NotImplementedException();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = new NavigationInfo();
            this.NavInfo.ElementsRequiredToLoad.Add(AcceptButton);
        }
    }
}