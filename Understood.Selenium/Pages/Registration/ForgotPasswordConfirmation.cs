﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Abstract;
using SeleniumFramework.Model;

namespace Understood.Selenium.Pages.Registration
{
    public class ForgotPasswordConfirmation : BasePage
    {
        [FindsBy(How = How.CssSelector, Using = "//*[@id='wrapper']/div[1]/div/div[2]/div/p[1]")]
        [CacheLookup]
        public IWebElement EmailAddressConfirmationText;

        protected override By GetValidationErrorBy()
        {
            throw new NotImplementedException();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = new NavigationInfo();
            NavInfo.ElementsRequiredToLoad.Add(EmailAddressConfirmationText);
        }
    }
}
