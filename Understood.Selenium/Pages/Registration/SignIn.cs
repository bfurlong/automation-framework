﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Model;
using SeleniumFramework.Model.UserModel;
using Understood.Selenium.Layouts;

namespace Understood.Selenium.Pages.Registration
{
    public class SignIn : SingleColumnLayout
    {
        //Test issue link
        [FindsBy(How = How.Id, Using = "main_0_uxEmailAddress")]
        [CacheLookup]
        public IWebElement UserNameInput;

        [FindsBy(How = How.Id, Using = "main_0_uxPassword")]
        [CacheLookup]
        public IWebElement UserPasswordInput;

        [FindsBy(How = How.Id, Using = "main_0_uxSignIn")]
        [CacheLookup]
        public IWebElement LoginSubmit;

        [FindsBy(How = How.ClassName, Using = "fb-sign-in")]
        [CacheLookup]
        public IWebElement FacebookLogin;

        [FindsBy(How = How.Id, Using = "main_0_uxForgotPassword")]
        [CacheLookup]
        public IWebElement ForgetPasswordLink;

        public void LoginAs(User user)
        {
            this.UserNameInput.SendKeys(user.Email);
            this.UserPasswordInput.SendKeys(user.Password);
            this.LoginSubmit.Click();
        }

        public ForgotPassword ForgotPassword()
        {
            return new ForgotPassword();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = new NavigationInfo();
            NavInfo.AddFindByCssClass(".sign-in");
            NavInfo.AddFindByTextContains("Sign");
            NavInfo.Path = "/thisshouldfail";
            NavInfo.ElementsRequiredToLoad.Add(UserNameInput);
        }

        protected override By GetValidationErrorBy()
        {
            return By.ClassName("validationerror");
        }
    }
}
