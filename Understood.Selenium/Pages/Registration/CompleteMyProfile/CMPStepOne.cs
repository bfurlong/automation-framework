﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeleniumFramework.Utilities;
using SeleniumFramework.Utilities.WebElementExtensions;
using Understood.Selenium.Model;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Model;
using Understood.Selenium.Layouts;
using Understood.Selenium.Utilities;

namespace Understood.Selenium.Pages.Registration.CompleteMyProfile
{
    // ReSharper disable once InconsistentNaming
    public class CMPStepOne : SingleColumnLayout
    {
        [FindsBy(How = How.ClassName, Using = "profile-questions")]
        [CacheLookup]
        public IWebElement ProfileQuestions;

        [FindsBy(How = How.Id, Using = "main_0_NextButton")]
        [CacheLookup]
        public IWebElement NextButton;

        private const string QuestionWrapperClass = ".question-wrapper";

        private const string AddChildQuestionClass = ".child-count-question";

        private const string RadioToggleWrapperClass = ".radio-toggle-wrapper";

        public IEnumerable<ChildQuestion> ChildQuestions { get; set; }

        public CMPStepOne()
        {
            var cq = new ChildQuestion();
            this.ChildQuestions = cq.GetChildQuestions();
        }
      
        public void AddChildren(IEnumerable<Child> children)
        {
            var childlist = (Stack<Child>)children;
            //TODO: I should probably make this a foreach.
            do
            {
                this.FillAddChildForm(childlist.Pop());
                this.AddChildForm(FormValueHelper.Yes);
            }
            while (childlist.Count != 0);
        }

        public void FillAddChildForm(Child child)
        {
            var cq = new ChildQuestion();
            cq.AnswerQuestion(child);
        } 

        public void AddChildForm(FormValueHelper val)
        {
            IWebDriver driver = DriverManager.Instance;
            IWebElement childCountQuestion = driver.FindElement(By.CssSelector(AddChildQuestionClass));

            if (childCountQuestion.Displayed)
            {
                var radioselect = new RadioButtons(childCountQuestion, 
                                                   By.CssSelector(RadioToggleWrapperClass));
                radioselect.SelectValue(val.ToString());
            }
        }

        public void SiblingQuestion(string siblingstatus)
        {
            var allquestions = ProfileQuestions.FindElements(By.CssSelector(QuestionWrapperClass));
            var questionelement = allquestions.Last();
            var radioselect = new RadioButtons(questionelement, 
                                               By.TagName(HTMLElements.label.ToString()));
            radioselect.SelectValue(siblingstatus);
        }

        protected override By GetValidationErrorBy()
        {
            throw new NotImplementedException();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = new NavigationInfo();
            this.NavInfo.Path = "/my-account/my-profile-step-one";
        }

        public CMPStepTwo SubmitForm()
        {
            NextButton.Click();
            return new CMPStepTwo();
        }
    }
}
