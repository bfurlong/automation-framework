﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Model;
using SeleniumFramework.Utilities.WebElementExtensions;
using Understood.Selenium.Layouts;
using Understood.Selenium.Model;

namespace Understood.Selenium.Pages.Registration.CompleteMyProfile
{
    // ReSharper disable once InconsistentNaming
    public class CMPStepTwo : SingleColumnLayout
    {
        [FindsBy(How = How.Id, Using = "main_0_ScreenNameTextBox")]
        [CacheLookup]
        public IWebElement ChildName;

        [FindsBy(How = How.CssSelector, Using = ".evaluation-question")]
        [CacheLookup]
        public IWebElement FieldSet;

        public void AddChildDetails(Child child)
        {
            ChildName.SendKeys(child.Name);
            this.FillChildIssueForm(child.ChildIssues);
        }

        private void FillChildIssueForm(IEnumerable<ChildIssues> childissues)
        {
            var rb = new RadioButtons(FieldSet, 
                                      By.TagName(HTMLElements.label.ToString()));

            var intersect = rb.Options.Where(
                elem => childissues.Any(ci => ci.ToString() == elem.Text)).ToList();

            intersect.ForEach(elem => elem.Click());
        }

        protected override By GetValidationErrorBy()
        {
            throw new System.NotImplementedException();
        }

        public override void CreateNavigationInfo()
        {
            NavInfo = null;
            
        }
    }
}
