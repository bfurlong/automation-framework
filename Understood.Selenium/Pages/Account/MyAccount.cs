﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Understood.Selenium.Pages.Account
{
    public class MyAccount
    {
        [FindsBy(How = How.Id, Using = "uploadAvatar")]
        [CacheLookup]
        public IWebElement AvatarUpload;

        [FindsBy(How = How.Id, Using = "main_0_imgAvatar")]
        [CacheLookup]
        public IWebElement UserAvatar;

        [FindsBy(How = How.CssSelector, Using = ".notifications-link")]
        [CacheLookup]
        public IWebElement NotificationsButton;

        [FindsBy(How = How.CssSelector, Using = "profile-link")]
        [CacheLookup]
        public IWebElement ProfileButton;

    }
}
