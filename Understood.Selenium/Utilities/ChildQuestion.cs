﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumFramework.Model;
using SeleniumFramework.Utilities;
using SeleniumFramework.Utilities.WebElementExtensions;
using Understood.Selenium.Model;

namespace Understood.Selenium.Utilities
{
    public class ChildQuestion
    {
        private const string QuestionCssClass = ".profile-questions-child-wrapper";

        public RadioButtons GenderSelection { get; set; }

        public SelectElement GradeSelection { get; set; }

        public IEnumerable<ChildQuestion> GetChildQuestions()
        {
            var childQuestions = new List<ChildQuestion>();
            
            foreach (var elem in this.getChildQuestionElements())
            {
                if (elem.Displayed)
                {
                    IWebElement select = elem.FindElement(By.TagName(HTMLElements.select.ToString()));
                    var cq = new ChildQuestion
                                 {
                                     GenderSelection = new RadioButtons(elem, By.TagName(HTMLElements.label.ToString())),
                                     GradeSelection = new SelectElement(select)
                                 };
                    childQuestions.Add(cq);
                }
            }
            return childQuestions;
        }

        private IReadOnlyCollection<IWebElement> getChildQuestionElements()
        {
            return DriverManager.Instance.FindElements(By.CssSelector(QuestionCssClass));
        }

        public void AnswerQuestion(Child child)
        {
            
        }



    }
}
