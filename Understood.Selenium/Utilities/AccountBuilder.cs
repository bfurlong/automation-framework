﻿using System;
using Understood.Selenium.Model;

namespace Understood.Selenium.Utilities
{
    public class AccountBuilder
    {
        public int NumberOfChildren { get; set; }
        public Boolean ValidEmail { get; set; }
        public Boolean ValidName { get; set; }
        public Boolean ValidPassword { get; set; }

        public PosesUser BuildAccount()
        {
            var user = new PosesUser();

            user.Email = this.createEmail(ValidEmail);
            user.FirstName = this.createName(ValidName);
            user.Password = this.createPassword(ValidPassword);
            user.PasswordConfirm = user.Password;

            return user;
        }

        private Child createChild(bool isvalid)
        {
            return new Child();
        }

        //TODO: add email slug to config file
        private string createEmail(bool isvalid)
        {
            var rand = new Random();
            var alias = rand.Next(100, 1000000);
            var email = "bfurlongao+" + alias + "@gmail.com";

            return email;
        }

        private string createName(bool isvalid)
        {
            //TODO: create name list in config file
            return "Bryan";
        }

        private string createPassword(bool isvalid)
        {
            //TODO: implement 
            return "password";
        }

    }
}
