﻿using System;
using OpenQA.Selenium;
using SeleniumFramework.Abstract;
using SeleniumFramework.Model;

namespace SeleniumFramework.Utilities
{
    public class NavigationService
    {
        ////TODO: Write detailed documentation
        private INavigatable _page;
        private IWebDriver driver;
        public NavigationService(INavigatable page)
        {
            _page = page;
            driver = DriverManager.Instance;
        }

        public void PerformInitialNavigation()
        {
            //TODO: Consider refactoring all of the get navinfo calls and calling once and storing private instance
            if (_page.GetNavigationInfo().Path != null)
            {
                this.GoTo(_page);
            }
            else if (driver.Url != "about:blank" && _page.GetNavigationInfo().Path == null)
            {
                this.NavigateTo(_page);
            }
            //TODO: Fix about blank issue
            else if (driver.Url == "about:blank")
            {
                this.GoTo(_page);
                this.NavigateTo(_page);
            }
            else if (_page.GetNavigationInfo().Path == null)
            {
                driver.Url = NavigationInfo.Slug;
                driver.Navigate();
                this.NavigateTo(_page);
            }

        }

        private void GoTo<T>(T page) where T : INavigatable
        {
            driver.Url = page.GetNavigationInfo().Url();
            driver.Navigate();
            try
            {
                _page.InitPage();
            }
            //TODO: fix generic exception catch
            catch (Exception e)
            {
                Console.WriteLine(DriverManager.Instance.Url + "Goto");
                _page.GetNavigationInfo().Path = null;
                this.PerformInitialNavigation();
            }
            finally { }
        }
        public void NavigateTo<T>(T page) where T : INavigatable
        {
            //var nvinfo = page.GetNavigationInfo();
            var searchElements = page.GetNavigationInfo().SearchElements;
            //TODO: refactor this need a cleaner way to handle this condition
            if (searchElements.Count == 0)
            {
                try
                {
                    page.InitPage();
                }
                catch (Exception e)
                {
                    
                }
            }
            else
            {
                foreach (By se in searchElements)
                {
                    try
                    {
                        IWebElement e = driver.FindElement(se);
                        e.Click();
                        page.InitPage();
                        break;
                    }
                    catch (Exception e)
                    {
                        //TODO: Add logging
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
    }
}
