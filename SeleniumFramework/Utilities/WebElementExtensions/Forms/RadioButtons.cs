﻿using System.Collections.Generic;
using OpenQA.Selenium;
using System.Linq;
namespace SeleniumFramework.Utilities.WebElementExtensions
{
 
    public class RadioButtons
    {
        public IReadOnlyCollection<IWebElement> Options; 

        public RadioButtons(IWebElement elementContext,
                            By setBy, 
                            By selectionBy)
        {
            Options = elementContext.FindElements(setBy);
            var select = elementContext.FindElement(selectionBy);
            select.Click();
        }

        public RadioButtons(IWebElement elementContext, By setBy)
        {
            Options = elementContext.FindElements(setBy);
        }

        public IDictionary<IWebElement, string> GetRadioValues()
        {
            //TODO: value attribue may need to be refactored
            return this.Options.ToDictionary(elem => elem, 
                        elem => elem.GetAttribute("value"));
        }

        public void SelectValue(string selection)
        {
            foreach (var elem in this.GetRadioValues().Where(elem => elem.Value == selection))
            {
                elem.Key.Click();
                break;
            }
        }
    }
}
