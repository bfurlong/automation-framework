﻿using System.Collections.Generic;
using OpenQA.Selenium;
using System.Linq;

namespace SeleniumFramework.Utilities.WebElementExtensions
{
    public class RadioButtons
    {
        //TODO: Clean up the logic and decide how to handle selection situations effectively
        //TODO: consider XPath helper utility
        public IReadOnlyCollection<IWebElement> Options;

        public IWebElement ElementContext;

        public RadioButtons(IWebElement elementContext,
                            By setBy,
                            By selectionBy)
        {
            this.ElementContext = elementContext;
            Options = elementContext.FindElements(setBy);
            var select = elementContext.FindElement(selectionBy);
            select.Click();
        }

        public RadioButtons(IWebElement elementContext, By setBy)
        {
            this.ElementContext = elementContext;
            Options = elementContext.FindElements(setBy);
        }

        public IDictionary<IWebElement, string> GetRadioValues()
        {
            //TODO: value attribue may need to be refactored
            return this.Options.ToDictionary(elem => elem, 
                        elem => elem.Text);
        }

        public void SelectValue(string selection)
        {
            foreach (var elem in this.GetRadioValues().Where(elem => elem.Value == selection))
            {
                elem.Key.Click();
                break;
            }
        }

        public void SelectBy(string elem, string contains)
        {
            //TODO: Consider moving this into an XPath helper utility
            var xpath = ElementContext.TagName+ "/" + elem + "[contains(text(),'" + contains + "')]";
           IWebElement selectelem = ElementContext.FindElement(By.XPath(xpath));
           
           
           selectelem.Click();
        }
    }
}
