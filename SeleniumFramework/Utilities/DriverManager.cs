﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFramework.Utilities
{
    public enum DriverName
    {
        Firefox,
        Chrome
    }

    public class DriverManager
    {
        private static IWebDriver instance = null;

        public static IWebDriver Instance
        {
            get
            {
                if (instance == null)
                {
                    //TODO: Fix this
                    throw new Exception("make this a specific exeption");
                }

                return instance;
            }
        }

        public static void SetInstance(DriverName driverName)
        {
            switch (driverName)
            {
                case DriverName.Chrome:
                    instance = new ChromeDriver();
                    break;

                case DriverName.Firefox:
                    instance = new FirefoxDriver();
                    break;
            }
        }

        public static void SetImplicitWait(TimeSpan timespan)
        {
            Instance.Manage().Timeouts().ImplicitlyWait(timespan);
            WebDriverWait wait = new WebDriverWait(Instance, timespan);
            
        }

      

    }
}
