﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace SeleniumFramework.Utilities
{
    public class ErrorUtility
    {
        public static IEnumerable<IWebElement> GetErrors(IWebDriver driver, By findby)
        {
            return driver.FindElements(findby);
        }
    }
}
