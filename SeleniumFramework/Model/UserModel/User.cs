﻿using MongoRepository;

namespace SeleniumFramework.Model.UserModel
{
    //TODO: consider making this abstract
    public class User :Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string Email { get; set; }
    }
}
