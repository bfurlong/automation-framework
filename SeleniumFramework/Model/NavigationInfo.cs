﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace SeleniumFramework.Model
{
    public class NavigationInfo
    {

        public static string Slug { get; set; }
        public string Path { get; set; }
        public string Language { get; set; }
        public List<By> SearchElements;
        public List<IWebElement> ElementsRequiredToLoad;

        public NavigationInfo()
        {
            //TODO: move into XML file
            Slug = "http://qa.understood.org";
            Language = "";
            SearchElements = new List<By>();
            ElementsRequiredToLoad = new List<IWebElement>();
        }

        public void AddFindByCssClass(string css)
        {
            SearchElements.Add(By.CssSelector(css));
        }

        public void AddFindById(string id)
        {
            SearchElements.Add(By.Id(id));
        }

        public void AddFindByXPath(string xpath)
        {
            SearchElements.Add(By.XPath(xpath));
        }

        public void AddFindByTextContains(string text)
        {
            SearchElements.Add(By.PartialLinkText(text));
        }

        public string Url()
        {
            return Slug + Language + Path;
        }
    }
}
