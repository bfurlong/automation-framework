﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumFramework.Model;
using SeleniumFramework.Utilities;

namespace SeleniumFramework.Abstract
{
    public abstract class BasePage : INavigatable
    {

        public NavigationService NavService { get; set; }
        protected NavigationInfo NavInfo;

        protected BasePage()
        {
            this.NavService = new NavigationService(this);
            CreateNavigationInfo();
            this.NavService.PerformInitialNavigation();
        }

        public NavigationInfo GetNavigationInfo()
        {
            return this.NavInfo;
        }

        public void InitPage()
        {
            PageFactory.InitElements(DriverManager.Instance, this);
            this.IsLoaded();
        }

        public IEnumerable<IWebElement> GetErrors()
        {
            return ErrorUtility.GetErrors(DriverManager.Instance, GetValidationErrorBy());
        }

        public void IsLoaded()
        {
            if (this.NavInfo.ElementsRequiredToLoad.Count < 1)
            {
                //TODO: fix the exceptions 
                throw new Exception(DriverManager.Instance.Url+"IsLoaded2");
            }
        }

        protected abstract By GetValidationErrorBy();

        public abstract void CreateNavigationInfo();
    }
}
