﻿using SeleniumFramework.Model;

namespace SeleniumFramework.Abstract
{
    public interface INavigatable
    {
        void CreateNavigationInfo();
        NavigationInfo GetNavigationInfo();
        void InitPage();
        void IsLoaded();
    }
}
