﻿Feature: SignIn
	Sign In

@mytag
Scenario: As a user with an account
	Given I am a user with a registered account 
	| Email                    | Password |
	| bfurlongao+22@gmail.com  | zzzzzz   |
	When I press login
	Then I should be redirected to the page I was on

Scenario: As a user without an account who tries to login
	Given I am a user without an account
	| Email                   | Password |
	| bfurlongao+99@gmail.com | zzzzzz   |
	When I press login
	Then I should be displayed an error saying "The account does not exist"
