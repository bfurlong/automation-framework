﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumFramework.Utilities;
using Understood.Selenium.Pages.Registration;
using SeleniumFramework.Model.UserModel;

namespace Understood.Test.RegistrationTests
{
    [TestClass]
    public class SignInTest
    {
        private User validUser;

        private User invalidUserPassword;

        [TestInitialize]
        public void TestSetup()
        {
            DriverManager.SetInstance(DriverName.Firefox);

            validUser = new User { Email = "bfurlongao+22@gmail.com", Password = "zzzzzz" };
            invalidUserPassword = new User { Email = "bfurlongao+22@gmail.com", Password = "password" };
        }

        [TestMethod]
        public void SignInExpectingSuccess()
        {
            var signin = new SignIn();
            var signinurl = DriverManager.Instance.Url;
            signin.LoginAs(validUser);
            Assert.AreNotEqual(signinurl, DriverManager.Instance.Url);
        }

        public void SignInExpectingFailure()
        {
            var signin = new SignIn();
            signin.LoginAs(invalidUserPassword);
            Assert.IsNotNull(signin.GetErrors());
        }

    }
}