﻿using TechTalk.SpecFlow;
using SeleniumFramework.Model.UserModel;
using SeleniumFramework.Utilities;
using TechTalk.SpecFlow.Assist;
using Understood.Selenium.Pages.Registration;

namespace Understood.Test.RegistrationTests
{

    

    [Binding]
    public class SignInSteps
    {
        [Given(@"I am a user with a registered account")]
        public void GivenIAmAUserWithARegisteredAccount(Table table)
        {
            DriverManager.SetInstance(DriverName.Firefox);
            var user = table.CreateInstance<User>();
            var signin = new SignIn();
            signin.LoginAs(user);

        }

        [When(@"I press login")]
        public void WhenIPressLogin()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should be redirected to the page I was on")]
        public void ThenIShouldBeRedirectedToThePageIWasOn()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"I am a user without an account")]
        public void GivenIAmAUserWithoutAnAccount(Table table)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should be displayed an error saying ""(.*)""")]
        public void ThenIShouldBeDisplayedAnErrorSaying(string p0)
        {
            ScenarioContext.Current.Pending();
        }

    }
}
