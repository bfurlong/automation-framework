﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumFramework.Model.UserModel;
using SeleniumFramework.Utilities;
using Understood.Selenium.Pages.Registration;

namespace Understood.Test.RegistrationTests
{
    using System;

    using Understood.Selenium.Pages.Core;

    [TestClass]
    public class SignIn
    {
        private User validUser;
        private User invalidUserPassword;

        [TestInitialize]
        public void TestSetup()
        {
            DriverManager.SetInstance(DriverName.Firefox);
            DriverManager.SetImplicitWait(TimeSpan.FromSeconds(5));

            validUser = new User { UserName = "bfurlongao+22@gmail.com", Password = "zzzzzz" };
            invalidUserPassword = new User { UserName = "bfurlongao+22@gmail.com", Password = "password" };
        }

        [TestMethod]
        public void SignInExpectingSuccess()
        {
            var signin = new SignIn();
            string signinurl = DriverManager.Instance.Url;
            signin.LoginAs(validUser);
            Assert.AreNotEqual(signinurl, DriverManager.Instance.Url);
        }

        public void SignInExpectingFailure()
        {
            var signin = new SignIn();
            signin.LoginAs(invalidUserPassword);
            Assert.IsNotNull(signin.GetErrors());
        }

        [TestMethod]
        public void SignInCompleteYourProfile()
        {
            var homepage = new HomePage();
            var signup =  homepage.CompleteYourProfile();
            var signin = signup.SignIn();
            signin.LoginAs(validUser);
        }
    }
}
