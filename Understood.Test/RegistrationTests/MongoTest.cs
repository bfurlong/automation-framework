﻿using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Understood.Selenium.Model;
using MongoRepository;

namespace Understood.Test
{
    [TestClass]
    public class UnitTest1
    {
        private static MongoRepository<PosesUser> userrepo;
        [TestInitialize]
        public void TestInit()
        {
            userrepo = new MongoRepository<PosesUser>();
        }
        [TestMethod]
        public void TestMethod1()
        {
            var user = new PosesUser();
            user.Email = "bfurlongao@gmail.com";
            user.FirstName = "Bryan";
            userrepo.Add(user);

            var mongouser = userrepo.First(u => u.Email == "bfurlongao@gmail.com");
            mongouser.FirstName = "Bryan2";
            userrepo.Update(mongouser);

            var mongouser2 = userrepo.First(u => u.Email == "bfurlongao@gmail.com");
            Console.WriteLine(mongouser2.FirstName);


        }
    }
}
