﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumFramework.Utilities;
using Understood.Selenium.Model;
using Understood.Selenium.Pages.Registration;
using Understood.Selenium.Pages.Registration.CompleteMyProfile;
using Understood.Selenium.Utilities;

namespace Understood.Test.RegistrationTests
{
    [TestClass]
    public class CompleteMyProfile
    {
        private CMPStepOne cmp;

        private PosesUser user;

        private Child child;

        [TestInitialize]
        public void TestInit()
        {

            DriverManager.SetInstance(DriverName.Firefox);

            DriverManager.SetImplicitWait(TimeSpan.FromSeconds(10));

            var accountBuilder = new AccountBuilder();

            user = accountBuilder.BuildAccount();

            var signup = new SignUp();
            var tc = signup.RegisterUser(user);
            tc.AcceptTerms();

            child = new Child();
            var childissues = new List<ChildIssues>();
            childissues.Add(ChildIssues.Attention);
            childissues.Add(ChildIssues.Attention);
            child.ChildIssues = childissues;
            child.Name = "John Jr";

            cmp = new CMPStepOne();
        }

        [TestMethod]
        public void AddChildTest()
        {
            var cqs = new List<ChildQuestion>(this.cmp.ChildQuestions);
            ChildQuestion cq = cqs.First();
            //TODO: Clean this up. 
            cq.GenderSelection.SelectValue(Gender.Girl.ToString());
            cq.GradeSelection.SelectByText("Grade 1");
            cmp.SiblingQuestion("Yes");
            var steptwo = cmp.SubmitForm();
            steptwo.AddChildDetails(child);
        }
    }
}
