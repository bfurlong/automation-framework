﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumFramework.Utilities;
using System;
using Understood.Selenium.Model;
using Understood.Selenium.Pages.Registration;

namespace Understood.Test.RegistrationTests
{
    [TestClass]
    public class ForgotPasswordTest
    {
        private PosesUser validUser;
        [TestInitialize]
        public void TestSetup()
        {
            DriverManager.SetInstance(DriverName.Firefox);
            validUser = new PosesUser() { Email = "bfurlongao+22@gmail.com"};
        }

        [TestMethod]
        public void ForgotPasswordSuccess()
        {
            //TODO: This method is incomplete need to fix the null element bug
            var signinPage = new SignIn();
            var forgotPasswordPage = signinPage.ForgotPassword();
            
            forgotPasswordPage.PasswordRequest(validUser);

            var forgotPasswordConfirmPage = new ForgotPasswordConfirmation();
            Console.WriteLine(forgotPasswordConfirmPage.EmailAddressConfirmationText.Displayed);
            Assert.IsNotNull("Hello");  
        }
    }
}
