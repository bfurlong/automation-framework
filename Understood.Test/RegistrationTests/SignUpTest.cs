﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumFramework.Utilities;
using Understood.Selenium.Model;
using Understood.Selenium.Pages.Registration;

namespace Understood.Test.RegistrationTests
{
    [TestClass]
    public class SignUpTest
    {
        private PosesUser validUser;
        [TestInitialize]
        public void TestSetup()
        {
            DriverManager.SetInstance(DriverName.Firefox);

            validUser = new PosesUser {
                                FirstName = "Tom",
                                Email = "bfurlongao+002@gmail.com",
                                Password = "password",
                                PasswordConfirm = "password",
                                ZipCode = "02459"
                            };
        }

        [TestMethod]
        public void RegisterNewUser()
        {
            var signup = new SignUp();
            signup.RegisterUser(validUser);
        }
    }
}
